﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityModel;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using IdentityServer4;

namespace IdentityServerWithAspNetIdentitySqlite
{


    public class ProfileService : IProfileService
    {

        public Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            //hardcoded them just for testing purposes
            List<Claim> claims = new List<Claim>() { new Claim("role", "free") };

            context.IssuedClaims = claims;


            return Task.FromResult(0);
        }

        public Task IsActiveAsync(IsActiveContext context)
        {
            var c = context;

            return Task.CompletedTask; 
        }
    }
}