﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;
using System.Collections.Generic;
using System.Security.Claims;

namespace QuickstartIdentityServer
{
    public class Config
    {
        // scopes define the resources in your system
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Address(),
                new IdentityResource("roles","your roles",new List<string>(){ "role"})
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
               // new ApiResource("api1", "My API"),


                new ApiResource("api1"){
                   UserClaims = { "role","address" }
                },




                //new ApiResource
                //{
                //    Name = "api1",
                //    DisplayName = "MY API 1" ,
                //    UserClaims = { JwtClaimTypes.Role  },
                //    Scopes =
                //    {
                //        new Scope()
                //        {
                //            Name = "roles",
                //             UserClaims = { "role" }
                //        }
                //    }

                // this API defines two scopes
                //Scopes =
                //{
                //    new Scope()
                //    {
                //        Name = "roles",
                //        DisplayName = "roles",
                //            UserClaims = { "role"} 
                //    },

                //}





                // new Scope
                //{
                //    Name = "api1",
                //    DisplayName = "API1 access",
                //    Description = "My API",
                //    Type = ScopeType.Resource,
                //    IncludeAllClaimsForUser = true,
                //    Claims = new List<ScopeClaim>
                //    {
                //        new ScopeClaim(ClaimTypes.Name),
                //        new ScopeClaim(ClaimTypes.Role)
                //    }
                //}
            };
        }

        // clients want to access resources (aka scopes)
        public static IEnumerable<Client> GetClients()
        {
            // client credentials client
            return new List<Client>
            {  
    

                // JavaScript Client
                new Client
                {
                    ClientId = "js",
                    ClientName = "JavaScript Client",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,
                    Enabled = true,
                    RedirectUris = { "http://localhost:5003/callback.html" },
                    PostLogoutRedirectUris = { "http://localhost:5003/index.html" },
                    AllowedCorsOrigins = { "http://localhost:5003" },
                    AlwaysIncludeUserClaimsInIdToken = true,
                     AlwaysSendClientClaims = true,
                    AccessTokenType = AccessTokenType.Jwt,
                    RequireConsent = false,
                     RequireClientSecret = false,
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Address,
                        "api1",
                        "roles"
                    },
                }
            };
        }

        public static List<TestUser> GetUsers()
        {
            return new List<TestUser>
            {
                new TestUser
                {
                    SubjectId = "1",
                    Username = "alice",
                    Password = "password",

                    Claims = new List<Claim>
                    {
                        new Claim("name", "Alice"),
                        new Claim("website", "https://alice.com"),
                        new Claim("address","2, big street"),
                        new Claim("role","free")
                    }
                },
                new TestUser
                {
                    SubjectId = "2",
                    Username = "bob",
                    Password = "password",

                    Claims = new List<Claim>
                    {
                        new Claim("name", "Bob"),
                        new Claim("website", "https://bob.com"),
                         new Claim("address","2, big street"),
                         new Claim("role","paying")
                    }
                }
            };
        }
    }
}